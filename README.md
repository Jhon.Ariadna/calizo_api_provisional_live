# calizo_api_provisional

Endpint o direccion de api

https://ec2-35-171-24-26.compute-1.amazonaws.com/api/data/


# secret_key de api SharpSpring
la llave o el secret_key.json se encuentra en el grupo de ariadna capeta de calizo, se debe alojar dentro de la capeta api_calizo:

'calizo_api_provisional\api_calizo\'


## Acceso al servidor

> Para acceder al servidor por medio de SSH se debe de contar con una conexión a través de la VPN de Ariadna la cual se solicita al departamento de TI de la agencia.

para ingresar a la instancia de AWS debe de descargar el archivo de permisos `calizo.pem` que se encuentra en el mismo folder en donde se encuentra este archivo, en teams.

Una vez descargado el archivo debe de abrir un terminal (cmd, powershel, etc.) dirigirse a la carpeta en donde descargo las credenciales y ejecutar el siguiente comando.

```ssh -i "calizo.pem" ec2-user@ec2-35-171-24-26.compute-1.amazonaws.com```


