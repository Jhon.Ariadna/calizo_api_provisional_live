FROM python:3.10-alpine
 
ENV PYTHONUNBUFFERED 1 
ENV PYTHONDONTWRITEBYTECODE=1

RUN pip install --upgrade pip

RUN mkdir / calizo_api_provisional
WORKDIR /calizo_api_provisional

COPY requirements.txt /calizo_api_provisional

RUN apk add --remove-orphans --update --no-cache mysql-client mysql-dev jpeg-dev mariadb-dev gcc
RUN apk add --update --no-cache --virtual .tmp-build-deps \
            gcc libc-dev linux-headers musl-dev zlib zlib-dev 
RUN apk del .tmp-build-deps

RUN python -m pip install -r requirements.txt

COPY . /calizo_api_provisional/
